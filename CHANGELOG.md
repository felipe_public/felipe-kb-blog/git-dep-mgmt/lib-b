# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
### [1.0.1](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-b/compare/v1.0.0...v1.0.1) (2022-02-13)

## 1.0.0 (2022-02-13)


### Features

* adds function for multiplying values ([ca5a744](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-b/commit/ca5a74429bfd51ea80c6991c63279e27fdd9936c))


### Docs

* updates readme documentation ([ab6ff99](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-b/commit/ab6ff99931d2f09b7ee1ece4571db5534eab57d8))


### CI

* adds versioning configuration file ([faa65ad](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-b/commit/faa65ad343b44f057c176cc1a5204db9d31538c6))
